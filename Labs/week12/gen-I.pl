#!/usr/bin/perl -w

use Getopt::Std;

use strict;
use warnings;

sub tailof;

my (@args) = @ARGV;
my ($prog) = tailof $0;
my ($usage) = "usage:\t$prog [-h] [-n rows (columns)] [-m]\n";

my %args;
getopts("hn:m", \%args);

exists $args{'h'} && die "$usage\n";;

my $n =		$args{'n'} || 10;
my $minus=	$args{'m'} || 0; # 1 ==> use -1 on diag

my $diag =	($minus == 1) ? -1 : 1;

# formatz
for (my $i = 1; $i <= $n; $i++)
{
    print "$i\t$diag\n";
}

###### subs here

sub tailof {
    $_[0] =~ m!([^/]*)$!;
    $1;
}
